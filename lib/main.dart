import 'package:eclubtest/core/app_init.dart';
import 'package:eclubtest/core/navigation/route_navigator.dart';
import 'package:flutter/material.dart';

void main() {AppInit.initializeApp();

WidgetsFlutterBinding.ensureInitialized();
runApp(
  const MyApp(),
);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: router,
      title: 'eClub',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
