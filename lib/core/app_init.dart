import 'package:flutter_dotenv/flutter_dotenv.dart';

class AppInit {
  static Future<void> initializeApp() async {
    await dotenv.load(fileName: ".env");
  }
}
